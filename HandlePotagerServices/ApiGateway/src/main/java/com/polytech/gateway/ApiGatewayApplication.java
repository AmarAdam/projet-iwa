package com.polytech.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiGatewayApplication {

	public static void main(String[] args) {
		System.out.println("Micro-service API Gateway lancé !");
		SpringApplication.run(ApiGatewayApplication.class, args);
	}

}
