package com.polytech.semis.repository;

import com.polytech.semis.data.Semis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SemisRepository extends JpaRepository<Semis,Long> {

}
