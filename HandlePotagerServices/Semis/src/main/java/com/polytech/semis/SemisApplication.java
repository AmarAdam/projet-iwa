package com.polytech.semis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class SemisApplication {

	public static void main(String[] args) {
		System.out.println("Micro service Semis lancé !");
		SpringApplication.run(SemisApplication.class, args);
	}

}
