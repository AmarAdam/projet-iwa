package com.polytech.semis.service;

import com.polytech.semis.data.Semis;
import com.polytech.semis.repository.SemisRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SemisService {
    private final SemisRepository semisRepository;

    public SemisService(SemisRepository semisRepository) {
        this.semisRepository = semisRepository;
    }

    public List<Semis> getSemis(){
        return semisRepository.findAll();
    }

    public void addSemis(Semis semis){
        semisRepository.save(
            Semis.builder()
                    .Id(semis.getId())
                    .terre(semis.getTerre())
                    .nombre(semis.getNombre())
                    .variete(semis.getVariete())
                    .emplacement(semis.getEmplacement())
                    .date(LocalDateTime.now())
                    .potagerId(semis.getPotagerId())
                    .build()
        );
    }

    public void deleteSemis(Integer semisId){
        boolean exists = semisRepository.existsById(Long.valueOf(semisId));
        if(!exists){
            throw new IllegalStateException(
                    "Traitement with id "+semisId+" does not exists"
            );
        }
        semisRepository.deleteById(Long.valueOf(semisId));
    }
}
