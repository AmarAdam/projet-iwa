package com.polytech.semis.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Semis {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private Long Id;
    private String terre;
    private LocalDateTime date;
    private Integer nombre;
    private String variete;
    private String emplacement;
    private Integer potagerId;
}
