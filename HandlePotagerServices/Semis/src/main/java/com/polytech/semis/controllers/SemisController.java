package com.polytech.semis.controllers;

import com.polytech.semis.data.Semis;
import com.polytech.semis.service.SemisService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/semis")
@Slf4j
public class SemisController {

    @Autowired
    private final KafkaTemplate<String,Semis> kafkaTemplate;
    private final SemisService semisService;

    @GetMapping
    public List<Semis> getSemis(){
        System.out.println("ça tape");
        return semisService.getSemis();
    }

    @PostMapping
    public void addSemis(@RequestBody Semis semis){
        System.out.println("on publie un semis");
        kafkaTemplate.send("handlepotager1",semis);
        semisService.addSemis(semis);
    }

    @DeleteMapping(path="{semisId}")
    public void deleteSemis(@PathVariable("semisId") Integer semisId){
        semisService.deleteSemis(semisId);
    }

}
