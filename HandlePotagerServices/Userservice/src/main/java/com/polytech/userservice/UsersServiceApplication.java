package com.polytech.userservice;

import com.polytech.userservice.data.Role;
import com.polytech.userservice.data.Users;
import com.polytech.userservice.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class UsersServiceApplication {

	public static void main(String[] args) {
		System.out.println("User Service lancé !");
		SpringApplication.run(UsersServiceApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

	@Bean
	CommandLineRunner run(UserService userService){
		return args -> {
			userService.saveRole(new Role(null,"ROLE_USER"));
			userService.saveRole(new Role(null,"ROLE_ADMIN"));
			userService.saveRole(new Role(null,"ROLE_SUPER_ADMIN"));

			userService.saveUser(new Users(null, "John T", "john", "1234", new ArrayList<>()));
			userService.saveUser(new Users(null, "Tahar R", "tahar", "1234", new ArrayList<>()));
			userService.saveUser(new Users(null, "Rayane R", "rayane", "", new ArrayList<>()));

			userService.addRoleToUser("john","ROLE_USER");
			userService.addRoleToUser("tahar","ROLE_ADMIN");
		};
	}

}
