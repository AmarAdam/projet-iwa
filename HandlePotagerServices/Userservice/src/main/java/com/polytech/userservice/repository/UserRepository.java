package com.polytech.userservice.repository;

import com.polytech.userservice.data.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users, Long> {

    Users findByUsername(String username);

}
