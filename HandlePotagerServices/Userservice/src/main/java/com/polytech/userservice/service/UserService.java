package com.polytech.userservice.service;

import com.polytech.userservice.data.Role;
import com.polytech.userservice.data.Users;

import java.util.List;


public interface UserService {

    Users saveUser(Users user);
    Role saveRole(Role role);
    void addRoleToUser(String username, String roleName);
    Users getUser(String username);
    List<Users>getUsers();

}
