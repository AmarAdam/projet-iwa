package com.polytech.userservice.service;

import com.polytech.userservice.data.Role;
import com.polytech.userservice.data.Users;
import com.polytech.userservice.repository.RoleRepository;
import com.polytech.userservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("wéééééé "+username);
        Users user = userRepository.findByUsername(username);
        if(user == null){
            log.error("User not found : {}");
            throw new UsernameNotFoundException("User not found");
        }else{
            log.info("User found : {}", username);
        }

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });
        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(), authorities);
    }

    @Override
    public Users saveUser(Users user) {
        log.info("Enregistrement du user {}", user.getName());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public Role saveRole(Role role) {
        log.info("Enregistrement du rôle {}", role.getName());
        return roleRepository.save(role);
    }

    @Override
    public void addRoleToUser(String userName, String roleName) {
        log.info("Enregistrement du rôle {} dans le user {}", userName, roleName );
        Users user = userRepository.findByUsername(userName);
        Role role = roleRepository.findByName(roleName);
        user.getRoles().add(role);
    }

    @Override
    public Users getUser(String userName) {
        log.info("Récupération du user {}", userName);
        return userRepository.findByUsername(userName);
    }

    @Override
    public List<Users> getUsers() {
        log.info("Récupération des users");
        return userRepository.findAll();
    }

}
