package potagers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PotagersApplication {

    public static void main(String[] args){
        System.out.println("Microservice Potagers lancé !");
        SpringApplication.run(PotagersApplication.class, args);
    }

}
