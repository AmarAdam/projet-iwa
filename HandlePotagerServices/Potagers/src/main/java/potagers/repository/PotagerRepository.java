package potagers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import potagers.data.Potager;

import java.util.Optional;

@Repository
public interface PotagerRepository extends JpaRepository<Potager, Long> {

    // c'est du jpql
    @Query("SELECT p FROM Potager p WHERE p.email = ?1")
    Optional<Potager> findPotagerByEmail(String email);

}
