package potagers.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import potagers.data.Potager;
import potagers.repository.PotagerRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PotagerService {

    private final PotagerRepository potagerRepository;

    @Autowired
    public PotagerService(PotagerRepository potagerRepository) {
        this.potagerRepository = potagerRepository;
    }

    public List<Potager> getPotagers(){
        return potagerRepository.findAll();
    }

    public void addPotager(Potager potager) {
        Optional<Potager> potagerByEmail = potagerRepository.findPotagerByEmail(potager.getEmail());
        if (potagerByEmail.isPresent()){
            throw new IllegalStateException("email associé à un potager");
        }
        potagerRepository.save(potager);

        System.out.println("Potager ajouté");
        System.out.println(potager);
    }

    public void deletePotager(Long potagerId) {
        System.out.println("Service, delete moi tout ça");
        boolean exists = potagerRepository.existsById(potagerId);
        if (!exists){
            throw new IllegalStateException(
                "Potager with id "+potagerId+" does not exists"
            );
        }
        potagerRepository.deleteById(potagerId);
    }

    /*
    on utilise pas requête ici, mais l'annotation transac, l'entity passe dans un état
    géré
     */
    @Transactional
    public void updatePotager(Long potagerId,
                              String name,
                              String email) {
        Potager potager = potagerRepository.findById(potagerId)
                .orElseThrow(() -> new IllegalStateException(
                        "Potager avec l'id "+potagerId+" n'existe pas"
        ));

        System.out.println(potager + " " + name + " " + email);

        if ( name != null && name.length() > 0 && !Objects.equals(potager.getName(), name) ){
            System.out.println("on est dans le set name");
            potager.setName(name);
        }

        if (email != null && email.length() > 0 && !Objects.equals(potager.getEmail(), email)){
            Optional<Potager> potagerOptional = potagerRepository.findPotagerByEmail(email);

            if(potagerOptional.isPresent()){
                throw new IllegalStateException("cet email est déjà utilisé");
            }

            System.out.println("on est dans le set email");

            potager.setEmail(email);
        }
    }

}
