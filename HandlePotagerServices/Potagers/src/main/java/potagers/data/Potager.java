package potagers.data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class Potager {

    @Id
    @SequenceGenerator(
            name = "potager_sequence",
            sequenceName = "potager_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "potager_sequence"
    )
    private Long id;
    private String name;
    private String email;
    @Transient
    private LocalDate doc;

    public Potager(){
        super();
    }

    public Potager(Long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.doc = doc;
    }

    public Potager(String name, String email){
        this.name = name;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDoc() {
        return LocalDate.now();
    }

    public void setDoc(LocalDate doc) {
        this.doc = doc;
    }

    @Override
    public String toString() {
        return "Potager{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", doc=" + doc +
                '}';
    }

}
