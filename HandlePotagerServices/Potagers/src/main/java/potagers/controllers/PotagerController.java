package potagers.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import potagers.service.PotagerService;
import potagers.data.Potager;

//import org.springframework.kafka.core.KafkaTemplate;

import java.util.List;

@RestController
@RequestMapping(path = "potagers")
public class PotagerController {

    private final PotagerService potagerService;

    @Autowired // le potagerService sera instancié est ajouté automatiquement
    public PotagerController(PotagerService potagerService) {
        this.potagerService = potagerService;
    }

    @GetMapping("/gateway")
    public String TestGateway(){
        System.out.println("Tavest");
        //kafkaTemplate.send(TOPIC, "on pète le Kafka");
        return "We weeeeeee";
    }

    @GetMapping
    public List<Potager> getPotagers(){
        System.out.println("Liste des potagers...");
        return potagerService.getPotagers();
    }

    @PostMapping
    public void addNewPotager(@RequestBody Potager potager){
        System.out.println("Controller Potagers, on poste un potager !");
        potagerService.addPotager(potager);
    }

    @DeleteMapping(path = "{potagerId}")
    public void deletePotager(@PathVariable("potagerId") Long potagerId){
        System.out.println("Controller Potagers, on jette un potager !");
        potagerService.deletePotager(potagerId);
    }

    @PutMapping(path = "{potagerId}")
    public void updatePotager(
            @PathVariable("potagerId") Long potagerId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String email) {
        System.out.println("Controller Potagers, on modifie un potager !");
        potagerService.updatePotager(potagerId, name, email);
    }

}
