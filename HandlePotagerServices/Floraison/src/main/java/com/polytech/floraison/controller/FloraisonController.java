package com.polytech.floraison.controller;

import com.polytech.floraison.data.Floraison;
import com.polytech.floraison.service.FloraisonService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "floraison")
public class FloraisonController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private KafkaTemplate<String, Floraison> kafkaTemplateFloraison;
    private static final String TOPIC = "floraison";

    private final FloraisonService floraisonService;

    @GetMapping(path = "/test")
    public String TestGateway(){
        System.out.println("Tavest");
        kafkaTemplate.send(TOPIC, "Notification de Floraison !");
        return "Test";
    }

    @GetMapping
    public List<Floraison> getFloraison(){
        return floraisonService.getFloraison();
    }

    @PostMapping
    public void addFloraison(@RequestBody Floraison floraison){
        System.out.println("on publie une floraison");

        Integer semisId = floraison.getSemisId();

        kafkaTemplate.send("floraison","Floraison sur le Semis numéro "+semisId+" !");
        floraisonService.addFloraison(floraison);
    }

    @DeleteMapping(path="{floraisonId}")
    public void deleteFloraison(@PathVariable("floraisonId") Integer floraisonId){
        floraisonService.deleteFloraison(floraisonId);
    }

}
