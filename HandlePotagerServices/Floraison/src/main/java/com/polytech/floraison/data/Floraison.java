package com.polytech.floraison.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Floraison {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private Long Id;
    private LocalDateTime date;
    private Integer quantite;
    private Integer semisId;

}
