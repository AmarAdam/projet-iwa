package com.polytech.floraison.repository;

import com.polytech.floraison.data.Floraison;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FloraisonRepository extends JpaRepository<Floraison,Long> {

}
