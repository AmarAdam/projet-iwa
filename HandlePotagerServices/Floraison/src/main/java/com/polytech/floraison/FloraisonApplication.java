package com.polytech.floraison;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FloraisonApplication {

	public static void main(String[] args) {
		SpringApplication.run(FloraisonApplication.class, args);
	}

}
