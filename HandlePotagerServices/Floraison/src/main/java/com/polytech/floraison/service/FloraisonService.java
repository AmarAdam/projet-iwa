package com.polytech.floraison.service;

import com.polytech.floraison.data.Floraison;
import com.polytech.floraison.repository.FloraisonRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class FloraisonService {
    private final FloraisonRepository floraisonRepository;

    public FloraisonService(FloraisonRepository floraisonRepository) {
        this.floraisonRepository = floraisonRepository;
    }

    public List<Floraison> getFloraison(){
        return floraisonRepository.findAll();
    }

    public void addFloraison(Floraison floraison){
        floraisonRepository.save(
                Floraison.builder()
                        .Id(floraison.getId())
                        .date(LocalDateTime.now())
                        .quantite(floraison.getQuantite())
                        .semisId(floraison.getSemisId())
                .build());
    }

    public void deleteFloraison(Integer Id){
        boolean exists = floraisonRepository.existsById(Long.valueOf(Id));
        if(!exists){
            throw new IllegalStateException(
                    "Floraison with id "+Id+" does not exists"
            );
        }
        floraisonRepository.deleteById(Long.valueOf(Id));
    }
}
