package com.polytech.kafka;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaListeners {

    @KafkaListener(topics = "handlepotager",groupId = "groupId")
    void listener(String data){
        System.out.println("Listener received : "+data+ " en bref ");
    }
}
