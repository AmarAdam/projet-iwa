package com.polytech.kafka.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    // on crée notre topic
    @Bean
    public NewTopic PotagerHandlerTopic(){
        return TopicBuilder.name("handlepotager").build();
    }
}
