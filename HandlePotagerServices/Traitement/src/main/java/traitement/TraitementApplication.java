package traitement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TraitementApplication {
    public static void main(String[] args) {
        System.out.println("MicroService Traitement lancé !");
        SpringApplication.run(TraitementApplication.class, args);
    }
}
