package traitement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import traitement.data.Traitement;

public interface TraitementRepository extends JpaRepository<Traitement, Integer> {

}
