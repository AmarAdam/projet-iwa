package traitement.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.*;
import traitement.config.TraitementResponse;
import traitement.data.Traitement;
import traitement.service.TraitementService;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("traitement")
@AllArgsConstructor
public class TraitementController {

    private final TraitementService traitementService;

    @PostMapping(path = "{traitementId}")
    public void estTraite(@PathVariable("traitementId") Integer traitementId, @RequestParam(required = false) String typeTraitement){
        traitementService.estTraite(traitementId,typeTraitement);
    }

    @GetMapping(path="/list")
    public List<Traitement> getTraitement(){
        System.out.println("Liste des traitements...");
        return traitementService.getTraitements();
    }

    @PostMapping
    public void addTraitement(@RequestBody Traitement traitement){
        System.out.println("Controller Traitement, on poste un traitement !");
        traitementService.addTraitement(traitement);
    }

    @DeleteMapping(path="{traitementId}")
    public void deleteTraitement(@PathVariable("traitementId") Integer traitementId){
        System.out.println("Controller Traitement, on jette un traitement !");
        traitementService.deleteTraitement(traitementId);
    }

    @PutMapping(path="{traitementId}")
    public void updateTraitement(@PathVariable("traitementId") Integer traitementId){
        System.out.println("Controller Traitement, on modifie un traitement");
        traitementService.updateTraitement(traitementId);
    }

    @KafkaListener(topics = "handlepotager1",groupId = "groupId")
    public void listener(String data){
        Gson object = new Gson();
        JsonObject jsonObject = object.fromJson(data, JsonObject.class);
        String newterre = jsonObject.get("terre").toString().replaceAll("^\"|\"$", "");
        Integer newnombre = Integer.valueOf(jsonObject.get("nombre").toString());
        String newvariete = jsonObject.get("variete").toString().replaceAll("^\"|\"$", "");
        Integer potagerId = Integer.valueOf(jsonObject.get("potagerId").toString());

        Traitement traitement = new Traitement(potagerId, newterre, newnombre, newvariete);
        System.out.println(traitement);
        traitementService.addTraitement(traitement);
    }

}