package traitement.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Traitement {

    @Id
    @Column(name = "id")
    @SequenceGenerator(
            name= "traitement_id_seq",
            sequenceName = "traitement_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "traitement_id_seq"
    )
    private Integer id;
    private Integer potagerId;
    private Boolean estTraite;
    private String terre;
    private Integer nombre;
    private String variete;
    private LocalDateTime Date;
    private String typeTraitement;

    public Traitement(Integer potagerId, String newterre, Integer newnombre, String newvariete) {
        this.estTraite=false;
        this.Date=LocalDateTime.now();
        this.potagerId=potagerId;
        this.nombre=newnombre;
        this.variete=newvariete;
        this.terre=newterre;
    }


    public String getTypeTraitement() {
        return typeTraitement;
    }

    public void setTypeTraitement(String typeTraitement) {
        this.typeTraitement = typeTraitement;
    }

    public Integer getId() {
        return id;
    }

    public Integer getPotagerId() {
        return potagerId;
    }

    public Boolean getEstTraite() {
        return estTraite;
    }

    public LocalDateTime getDate() {
        return Date;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPotagerId(Integer potagerId) {
        this.potagerId = potagerId;
    }

    public void setEstTraite(Boolean estTraite) {
        this.estTraite = estTraite;
    }

    public void setDate(LocalDateTime date) {
        Date = date;
    }

}
