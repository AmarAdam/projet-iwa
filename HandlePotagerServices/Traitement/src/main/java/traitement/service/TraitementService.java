package traitement.service;

import org.springframework.stereotype.Service;
import traitement.data.Traitement;
import traitement.repository.TraitementRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class TraitementService {

    private final TraitementRepository traitementRepository;

    public TraitementService(TraitementRepository traitementRepository) {
        this.traitementRepository = traitementRepository;
    }

    @Transactional
    public void estTraite(Integer traitementId, String typeTraitement){
        Traitement traitement = traitementRepository.findById(traitementId)
                .orElseThrow(() -> new IllegalStateException(
                        "Traitement avec l'id "+traitementId+" n'existe pas"
                ));
        System.out.println(traitement + " "+traitementId);
        traitement.setEstTraite(true);
        traitement.setTypeTraitement(typeTraitement);
        System.out.println("EstTraite : "+traitement.getEstTraite()+" "+traitement.getTypeTraitement());
    }

    public List<Traitement> getTraitements(){ return traitementRepository.findAll();}

    public void addTraitement(Traitement traitement){
        traitementRepository.save(
                Traitement.builder()
                        .potagerId(traitement.getPotagerId())
                        .estTraite(traitement.getEstTraite())
                        .typeTraitement(traitement.getTypeTraitement())
                        .terre(traitement.getTerre())
                        .nombre(traitement.getNombre())
                        .variete(traitement.getVariete())
                        .Date(LocalDateTime.now())
                        .build()
        );
    }

    public void deleteTraitement(Integer traitementId){
        boolean exists = traitementRepository.existsById(traitementId);
        if(!exists){
            throw new IllegalStateException(
                    "Traitement with id "+traitementId+" does not exists"
            );
        }
        traitementRepository.deleteById(traitementId);
    }

    @Transactional
    public void updateTraitement(Integer traitementId) {
        Traitement traitement = traitementRepository.findById(traitementId)
                .orElseThrow(() -> new IllegalStateException(
                        "Traitement avec l'id "+traitementId+" n'existe pas"
                ));

        System.out.println(traitement + " "+traitementId);

        traitement.setEstTraite(true);
    }
}
