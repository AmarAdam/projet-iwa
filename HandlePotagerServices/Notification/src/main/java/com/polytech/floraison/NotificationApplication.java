package com.polytech.floraison;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
public class NotificationApplication {

	public static void main(String[] args) {
		System.out.println("MicroService Notification lancé !");
		SpringApplication.run(NotificationApplication.class, args);
	}

}
