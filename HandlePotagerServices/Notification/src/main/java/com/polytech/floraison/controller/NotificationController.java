package com.polytech.floraison.controller;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificationController {

    @KafkaListener(topics = "floraison",groupId = "groupId")
    void listener(String data){
        System.out.println("Listener activated !");
        System.out.println("Listener received : "+data);
    }
}
