//SCRIPT HANDLEPOTAGER

CREATE TABLE potager (
   idPotager BIGSERIAL PRIMARY KEY,
   namePotager VARCHAR(50),
   email VARCHAR(50)
);

CREATE TABLE semis (
   id BIGSERIAL PRIMARY KEY,
   terre VARCHAR(50),
   nombre SMALLINT,
   variete VARCHAR(50),
   emplacement VARCHAR(50),
   date DATE,
   potager_id FOREIGN KEY
);

CREATE TABLE traitement (
   id BIGSERIAL PRIMARY KEY,
   LocalDateTime Date,
   type_traitement VARCHAR(50),
   quantite SMALLINT,
   semis_id FOREIGN KEY
);

CREATE TABLE floraison (
   id BIGSERIAL PRIMARY KEY,
   terre VARCHAR(50),
   nombre SMALLINT,
   variete VARCHAR(50),
   emplacement VARCHAR(50),
   date DATE,
   potager_id FOREIGN KEY
);

CREATE TABLE users (
   id BIGSERIAL PRIMARY KEY,
   firstName VARCHAR(50),
   lastName VARCHAR(50),
   email VARCHAR(50)
);

CREATE TABLE users_roles (
   users_id SMALLINT,
   roles_id SMALLINT
);

CREATE TABLE role (
   id BIGSERIAL PRIMARY KEY,
   name VARCHAR(50)
);
